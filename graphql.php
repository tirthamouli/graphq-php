<?php

require_once __DIR__ . '/../../vendor/autoload.php';

use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\Type;
use GraphQL\Type\Schema;
use GraphQL\GraphQL;


$books = [
    [ 'name' => 'Name of the Wind', 'genre'=> 'Fantasy', 'id'=> 1, 'authorId'=> 1 ],
    [ 'name' => 'The Final Empire', 'genre'=> 'Fantasy', 'id'=> 2, 'authorId'=> 2 ],
    [ 'name' => 'The Hero of Ages', 'genre'=> 'Fantasy', 'id'=> 4, 'authorId'=> 2 ],
    [ 'name' => 'The Long Earth', 'genre'=> 'Sci-Fi', 'id'=> 3, 'authorId'=> 3 ],
    [ 'name' => 'The Colour of Magic', 'genre'=> 'Fantasy', 'id'=> 5, 'authorId'=> 3 ],
    [ 'name' => 'The Light Fantastic', 'genre'=> 'Fantasy', 'id'=> 6, 'authorId'=> 3 ],
];

$authors = [
    [ 'name'=> 'Patrick Rothfuss', 'age'=> 44, 'id'=> 1 ],
    [ 'name'=> 'Brandon Sanderson', 'age'=> 42, 'id'=> 2 ],
    [ 'name'=> 'Terry Pratchett', 'age'=> 66, 'id'=> 3 ]
];

try {

    $bookType = new ObjectType([
        'name' => 'Book',
        'fields' => function() use (&$authorType, $authors){
            return[
                'id' => [ 'type' => Type::id() ],
                'name' => [ 'type' => Type::string() ],
                'genre' => [ 'type' => Type::string() ],
                'author' => [
                    'type' => $authorType,
                    'description' => "Author of the book",
                    'resolve' => function($parent, $args) use($authors){
                        return array_values(array_filter($authors, function($author) use($parent) {
                            return $parent['authorId'] === $author['id'];
                        }))[0];
                    }
                ],
            ];
        }
    ]);

    $authorType = new ObjectType([
        'name' => 'Author',
        'fields' => function() use (&$bookType, $books){
            return[
                'id' => [ 'type' => Type::id() ],
                'name' => [ 'type' => Type::string() ],
                'age' => [ 'type' => Type::int() ],
                'books' => [
                    'type' => Type::listOf($bookType),
                    'resolve' => function($parent, $args) use($books){
                        return array_values(array_filter($books, function($book) use($parent) {
                            return $parent['id'] === $book['authorId'];
                        }));
                    }
                ],
            ];
        }
    ]);



    $queryType = new ObjectType([
        'name' => 'Query',
        'fields' => [
            'book' => [
                'type' => $bookType,
                'args' => [
                    'id' => ['type' => Type::id()]
                ],
                'resolve' => function ($root, $args) use($books) {
                    return array_values(array_filter($books, function($book) use($args) {
                        return (int)$args['id'] == $book['id'];
                    }))[0];
                }
            ],

            'author' => [
                'type' => $authorType,
                'args' => [
                    'id' => ['type' => Type::id()]
                ],
                'resolve' => function ($root, $args) use($authors) {
                    return array_values(array_filter($authors, function($author) use($args) {
                        return (int)$args['id'] == $author['id'];
                    }))[0];
                }
            ],

            'books' => [
                'type' => Type::listOf($bookType),
                'args' => [
                    'id' => ['type' => Type::id()]
                ],
                'resolve' => function ($root, $args) use($books) {
                    return $books;
                }
            ],

            'authors' => [
                'type' => Type::listOf($authorType),
                'args' => [
                    'id' => ['type' => Type::id()]
                ],
                'resolve' => function ($root, $args) use($authors) {
                    return $authors;
                }
            ]
        ],
    ]);

    $mutationType = new ObjectType([
        'name' => 'Calc',
        'fields' => [
            'sum' => [
                'type' => Type::int(),
                'args' => [
                    'x' => ['type' => Type::int()],
                    'y' => ['type' => Type::int()],
                ],
                'resolve' => function ($root, $args) {
                    return $args['x'] + $args['y'];
                },
            ],
        ],
    ]);

    // See docs on schema options:
    // http://webonyx.github.io/graphql-php/type-system/schema/#configuration-options
    $schema = new Schema([
        'query' => $queryType,
        'mutation' => $mutationType,
    ]);

    $rawInput = file_get_contents('php://input');
    $input = json_decode($rawInput, true);
    $query = $input['query'];
    $variableValues = isset($input['variables']) ? $input['variables'] : null;

    $rootValue = ['prefix' => 'You said: '];
    $result = GraphQL::executeQuery($schema, $query, $rootValue, null, $variableValues);
    $output = $result->toArray();
} catch (\Exception $e) {
    $output = [
        'error' => [
            'message' => $e->getMessage()
        ]
    ];
}

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Headers: content-type');
header('Content-Type: application/json; charset=UTF-8');
echo json_encode($output);

