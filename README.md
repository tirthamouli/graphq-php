### Run locally
```
php -S localhost:8080 ./graphql.php
```

### Try query
```
curl http://localhost:8000 -d '{"query": "query { books{name genre author{name age}} }" }'
```